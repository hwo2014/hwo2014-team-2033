package noobbot;

class YourCar {
	public final YourCarData data;
	YourCar (final YourCarData data) {
		this.data = data;
	}

	class YourCarData {
		public final String name;
		public final String color;
		YourCarData (final String name, final String color) {
			this.name = name;
			this.color = color;
		}
	}
}