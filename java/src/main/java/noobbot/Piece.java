package noobbot;

import noobbot.Main.Switchthingy;

class Piece {
	public Boolean isStraight;
	public final double length;
	public final double radius;
	public final float angle;
	public final Switchthingy hasSwitch;
	Piece (final Boolean isStraight, final int length, final int radius, final int angle, final Switchthingy hasSwitch) {
		this.isStraight = isStraight;
		this.length = length;
		this.radius = radius;
		this.angle = angle;
		this.hasSwitch = hasSwitch;
	}
}