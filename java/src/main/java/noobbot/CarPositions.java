package noobbot;

class CarPositions {
	public final String msgType;
	public final CarData[] data;
	public final int gameTick;
	CarPositions (final String msgType, final CarData[] data, final int gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}

	class CarData {
		public final ID id;
		public final float angle;
		public final PiecePosition piecePosition;
		public final int lap;
		CarData (final ID id, final float angle, final PiecePosition piecePosition, final int lap) {
			this.id = id;
			this.angle = angle;
			this.piecePosition = piecePosition;
			this.lap = lap;
		}
	
		class ID {
			public final String name;
			public final String color;
			ID (final String name, final String color) {
				this.name = color;
				this.color = color;
			}
		}
	
		class PiecePosition {
			public final int pieceIndex;
			public final float inPieceDistance;
			public final MyLane lane;
			PiecePosition (final int pieceIndex, final float inPieceDistance, final MyLane lane) {
				this.pieceIndex = pieceIndex;
				this.inPieceDistance = inPieceDistance;
				this.lane = lane;
			}
		
		}
	}
}