package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;

public class Main extends Thread {
	static Boolean fullDebug = false;
	
	public static void main(String... args) throws IOException {
		String host = "testserver.helloworldopen.com";
		int port = 8091;
		String botName = "Dragon Racer";
		String botKey = args[3];
		String trackName = "usa";
		int carCount = 1;
		String password = null;
		Boolean isCreateRace = false;
		int extraInstances = 0;
		
		switch(args.length){
		default:
			System.out.println("WARNING: some arguments may be ignored. This is a complete list: "+java.util.Arrays.toString(args));
		case 10:
			try{
				fullDebug = Boolean.parseBoolean(args[9]);
			} catch(Exception e) {};
		case 9:
			try{
				extraInstances = Integer.parseInt(args[8]);
			} catch(Exception e) {};
		case 8:
			try{
				isCreateRace = Boolean.parseBoolean(args[7]);
			} catch(Exception e) {};
		case 7:
			password = args[6];
		case 6:
			try{
				carCount = Integer.parseInt(args[5]);
			} catch(Exception e) {};
		case 5:
			trackName = args[4];
		case 4:
			botKey = args[3];
		case 3:
			botName = args[2];
		case 2:
			port = Integer.parseInt(args[1]);
		case 1:
			host = args[0];
		case 0:
			break;
		}

		if(args.length<=4){
			// JOIN

			System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + "\n"); //Default

			new Main(new Socket(host, port), new Join(botName, botKey)).start();
		} else {
			if (isCreateRace){
				// CREATERACE
				System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + "\n"); //Default

				Main m1 = new Main(new Socket(host, port), new CreateRace(new BotId(botName, botKey),trackName,password,carCount));
				m1.start();
				
				// JOINRACE of extra local instances
				if(0 < extraInstances){
					String[] names = {"Prima", "Dubio", "Trex", "Quasi", "Loco", "Flex", "Eve"};
					System.out.println("Waiting for race to be created...");
					while(!m1.isReady()){}
					System.out.println("Other instances getting ready.");
					for(int i=0; i<carCount && i < extraInstances && i<7; i++){

						System.out.println("Connecting to " + host + ":" + port + " as " + names[i] + "/" + botKey + "\n"); //Default

						new Main(new Socket(host, port), new JoinRace(new BotId(names[i], botKey),trackName,password,carCount)).start();
					}
				}
			}else{
				// JOINRACE
				System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + "\n"); //Default

				new Main(new Socket(host, port), new JoinRace(new BotId(botName, botKey),trackName,password,carCount)).start();
			}
		}

		System.out.println("main() Finished!");
	}

	final Gson gson = new Gson();
	private final PrintWriter writer;
	private final BufferedReader reader;
	private Boolean ready = false;

	public Boolean isReady() {
		return ready;
	}

	public Main(final Socket socket, final JoinRace sendMsg) throws IOException {
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		send(sendMsg);
	}

	public Main(final Socket socket, final CreateRace sendMsg) throws IOException {
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		send(sendMsg);
		ready = true;
	}

	public Main(final Socket socket, final Join sendMsg) throws IOException {
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		send(sendMsg);
	}

   @Override
   public void run()
   {
		try {
			mainStartYourEngines(reader,writer);
		} catch (IOException e) {
			System.err.println("Could not create thread to run multiple instances.");
			System.out.println("ERROR: Could not create thread to run multiple instances.");
			e.printStackTrace();
		}
   }
	
	public void mainStartYourEngines(final BufferedReader reader, final PrintWriter writer) throws IOException {
		String line = null;

		Ping ping = new Ping();

		String yourCarName = "noname";
		String yourCarColor = "unknown";
		int yourCarNum = 0; // The index in reply messages from server

		double angle = 0;
		double speed = 0;
		int pieceindex = 0;
		double inPieceDistance = 0;
		Piece[] board = null;
		int[] lanes = null;
		double maxangle = 0;
		double deltaspeed = 0;
		double friction = 0;//c1
		double acceleration = 0;//c2
		Boolean testing = true;
		double speed1 = 0;
		double deltaspeed1 = 0;
		double predictedspeed = 0;
		double time = 0;
		double angle1 = 0;
		double angle2 = 0;
		double speedangle2 = 0;
		double angle3 = 0;
		double speedangle3 = 0;
		double angle4 = 0;
		double c4 = 0;
		double c5 = 0;
		final double throttle = 1.0;

		/* x = time
		 * T = throttle
		 * V' : change in speed
		 * V : speed 
		 * D : travel distance in that time
		 */
		// V'=c1*V+c2*T-c3
		// V=e^(c1*x)-(c2*T)/c1-(c3/-c1) for deceleration
		// V=-e^(c1*x)-(c2*T)/c1-(c3/-c1) for acceleration
		// D=e^(c1*x)/c1-(c2*T*x)/c1-(c3*x/-c1) for deceleration
		// D=-e^(c1*x)/c1-(c2*T*x)/c1-(c3*x/-c1) for acceleration

		// c3 appears to be 0
		// stable speed=function with radius of corner

		/* A'' : acceleration in turn speed
		 * 
		 */
		// A''=c4*A+f(V,R) change in change in angle
		// f(V,R)=c5*(V^2)/R suspected

		System.out.println("starting");
		while((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				CarPositions message = null;
				try{message = gson.fromJson(line, CarPositions.class);}catch(Exception e){e.printStackTrace();}
				double newspeed;

				int length = message.data.length;
				for(int i = 0; i < length; i++){
					if(message.data[i].id.name.equals(yourCarName)){
						yourCarNum = i;
					}
				}

				if (message.data[yourCarNum].piecePosition.pieceIndex==pieceindex) {
					newspeed = message.data[yourCarNum].piecePosition.inPieceDistance-inPieceDistance;
				}
				else {
					// PIECE CHANGE
					if (board[pieceindex].length==0) {
						double R = board[pieceindex].radius;
						if (board[pieceindex].angle>0)
							R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
						else
							R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
						newspeed = message.data[yourCarNum].piecePosition.inPieceDistance+2*Math.PI*R*Math.abs(board[pieceindex].angle)/360-inPieceDistance;
					}
					else
						newspeed = message.data[yourCarNum].piecePosition.inPieceDistance+board[pieceindex].length-inPieceDistance;
					pieceindex = message.data[yourCarNum].piecePosition.pieceIndex;
				}
				deltaspeed = newspeed-speed;
				if (testing) {
					if (speed1==0) {
						if (speed+deltaspeed/2!=0&&deltaspeed>0) {
							speed1=speed-deltaspeed/2;
							deltaspeed1=deltaspeed;
						}
					}
					else if(deltaspeed>0&&acceleration==0) {
						double speed2 = speed-deltaspeed/2;
						double deltaspeed2 = deltaspeed;
						friction = (deltaspeed2-deltaspeed1)/(speed2-speed1);
						acceleration = (deltaspeed1-friction*speed1)/throttle;
						if(fullDebug) System.out.println(""+deltaspeed1+" "+deltaspeed2);
					}
					else if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle!=0) {
						if(angle1==0) {
							angle1 = message.data[yourCarNum].angle;
						}
						else if(angle2==0) {
							angle2 = message.data[yourCarNum].angle;
							speedangle2 = newspeed;
						}
						else if(angle3==0) {
							angle3 = message.data[yourCarNum].angle;
							speedangle3 = newspeed;
						}
						else if(angle4==0) {
							angle4 = message.data[yourCarNum].angle;
							double fx1 = (angle3-angle2)-(angle2-angle1);
							double fx2 = (angle4-angle3)-(angle3-angle2);
							//-c5*(V1^2)/R+c5*(V2^2)/R=fx2-fx1
							//c5(-v1^2/R+V2^2/R)=fx2-fx1
							double R = board[message.data[yourCarNum].piecePosition.pieceIndex].radius;
							if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle>0)
								R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
							else
								R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
							c5=R*(fx2-fx1)/(Math.pow(speedangle3, 2)-Math.pow(speedangle2, 2));
							c4=fx1-c5*(Math.pow(speedangle2, 2))/(R*angle2);
							testing = false;
						}
					}
				}
				else {
				}
				speed = newspeed;
				inPieceDistance = message.data[yourCarNum].piecePosition.inPieceDistance;
				if (testing) {
					send(new Throttle(throttle));
				}
				else {
					Boolean done = false;
					if (predictedspeed==0) { 
						predictedspeed = newspeed;
					} else {
						time = (Math.log((predictedspeed+0.0*acceleration/friction))/friction)+1;
						predictedspeed = Math.exp(friction*time)-0.0*acceleration/friction;
					}
					for(int i=0;i<board.length;i++) {
						if (board[i].angle!=0&&i!=message.data[yourCarNum].piecePosition.pieceIndex) {
							double distance = 0;
							if (i<message.data[yourCarNum].piecePosition.pieceIndex) {
								for (int j=i;j<message.data[yourCarNum].piecePosition.pieceIndex;j++) {
									if (board[j].length!=0)
										distance += board[j].length;
									else {
										double R = board[message.data[yourCarNum].piecePosition.pieceIndex].radius;
										if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle>0)
											R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
										else
											R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
										distance += 2*Math.PI*R*Math.abs(board[message.data[yourCarNum].piecePosition.pieceIndex].angle)/360;
									}
								}
							}
							else {
								for (int j=i;j<board.length;j++) {
									if (board[j].length!=0)
										distance += board[j].length;
									else {
										double R = board[message.data[yourCarNum].piecePosition.pieceIndex].radius;
										if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle>0)
											R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
										else
											R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
										distance += 2*Math.PI*R*Math.abs(board[message.data[yourCarNum].piecePosition.pieceIndex].angle)/360;
									}
								}
								for (int j=0;j<message.data[yourCarNum].piecePosition.pieceIndex;j++) {
									if (board[j].length!=0)
										distance += board[j].length;
									else {
										double R = board[message.data[yourCarNum].piecePosition.pieceIndex].radius;
										if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle>0)
											R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
										else
											R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
										distance += 2*Math.PI*R*Math.abs(board[message.data[yourCarNum].piecePosition.pieceIndex].angle)/360;
									}
								}
							}
							double R = board[message.data[yourCarNum].piecePosition.pieceIndex].radius;
							if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle>0)
								R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
							else
								R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
							if (distance*1.01<distance(speed,Math.sqrt(30*c4*R/c5),friction,acceleration)) {
								send(new Throttle(0.0));
								done = true;
								if(fullDebug) System.out.println("0.0 in preparation of corner "+speed);
							}
						}
					}
					if (!done&&board[message.data[yourCarNum].piecePosition.pieceIndex].angle==0) {
						send(new Throttle(1.0));
						if(fullDebug) System.out.println("1.0 in straight "+speed+" "+c4+" "+c5);
					}
					else if (!done) {
						double R = board[message.data[yourCarNum].piecePosition.pieceIndex].radius;
						if (board[message.data[yourCarNum].piecePosition.pieceIndex].angle>0)
							R -= lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
						else
							R += lanes[message.data[yourCarNum].piecePosition.lane.endLaneIndex];
						if (speed<Math.sqrt(-30*c4*R/c5)*0.95) {
							send(new Throttle(1.0));
							if(fullDebug) System.out.println("1.0 in corner "+speed+" "+(Math.sqrt(-30*c4*R/c5)*0.95));
						}
						else {
							send(new Throttle(0.0));
							if(fullDebug) System.out.println("0.0 in corner "+speed+" "+(Math.sqrt(-30*c4*R/c5)*0.95));
						}
					}
				}
				angle = Math.abs(message.data[yourCarNum].angle);
				if (angle>maxangle)
					maxangle = angle;
			} // END msgType == carPositions 
			else{
				// Incoming messages with Ping as reply
				if (msgFromServer.msgType.equals("join")) {
					System.out.println("Joined"); //Default
				} 
				else if (msgFromServer.msgType.equals("gameInit")) {
					GameInit message = gson.fromJson(line, GameInit.class);
					if (message.data!=null&&message.data.race!=null&&message.data.race.track!=null&&message.data.race.track.pieces!=null) {
						board = message.data.race.track.pieces;
						for (int i=0;i<board.length;i++) {
							board[i].isStraight = board[i].length!=0;
						}
						System.out.println("Board set, length:"+board.length);
					}
					lanes = new int[message.data.race.track.lanes.length];
					for (int i=0;i<lanes.length;i++) {
						lanes[i] = message.data.race.track.lanes[i].distanceFromCenter;
					}
					System.out.println("Race init"); //Default
				} 
				else if (msgFromServer.msgType.equals("gameEnd")) {
					System.out.println("Race end"); //Default
				} 
				else if (msgFromServer.msgType.equals("gameStart")) {
					System.out.println("Race start"); //Default
				} 
				else if (msgFromServer.msgType.equals("yourCar")) {
					// Just to be safe, store who the server thinks I am
					YourCar message = gson.fromJson(line, YourCar.class);
					yourCarName = message.data.name;
					yourCarColor = message.data.color;
					System.out.println("\"yourCar\" name:'"+yourCarName+"' color:'"+yourCarColor+"'");
				} 
				else {
					System.out.println("\""+yourCarName+"\"("+yourCarColor+"): "+msgFromServer.msgType);
				}
				send(ping);
			}
		}
		System.out.println("End (no data connection)");
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
	
	class Switchthingy {
		@SerializedName("switch")
		Boolean hasSwitch;
	}
	
	private double distance (double startspeed, double endspeed, double friction, double acceleration) {
		if (startspeed<=endspeed)
			return 0;
		double starttime = (Math.log((startspeed+0.0*acceleration/friction))/friction);
		double endtime = (Math.log((endspeed+0.0*acceleration/friction))/friction);
		//D=e^(c1*x)/c1-(c2*T*x)/c1
		return Math.exp(friction*endtime)/friction-Math.exp(friction*starttime)/friction;
	}
}

class Lane {
	public final int distanceFromCenter;
	public final int index;
	Lane (final int distanceFromCenter, final int index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
}

class MyLane {
	public final int startLaneIndex;
	public final int endLaneIndex;
	MyLane (final int startLaneIndex, final int endLaneIndex) {
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}
}

class BotId {
	public final String name;
	public final String key;
	BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}

class JoinRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final String password;
	public final int carCount;
	JoinRace(final BotId botId, final String trackName, final String password, final int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class CreateRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final String password;
	public final int carCount;
	CreateRace(final BotId botId, final String trackName, final String password, final int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class Switch extends SendMsg {
	private String value;

	public Switch (Boolean left) {
		if (left)
			this.value = "Left";
		else
			this.value = "Right";
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}