package noobbot;

class GameInit {
	public final Data data;
	GameInit (final Data data) {
		this.data = data;
	}

	class Data {
		public final Race race;
		Data (final Race race) {
			this.race = race;
		}
	
		class Race {
			public final Track track;
			public final Object cars;
			public final Object raceSession;
			Race (final Track track, final Object cars, final Object raceSession) {
				this.track = track;
				this.cars = cars;
				this.raceSession = raceSession;
			}
		
			class Track {
				public final String id;
				public final String name;
				public final Piece[] pieces;
				public final Lane[] lanes;
				public final Object startingPoint;
				Track (final String id, final String name, final Piece[] pieces, final Lane[] lanes, final Object startingPoint) {
					this.id = id;
					this.name = name;
					this.pieces = pieces;
					this.lanes = lanes;
					this.startingPoint = startingPoint;
				}
			}
		}
	}
}